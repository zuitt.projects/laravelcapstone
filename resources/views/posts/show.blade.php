@extends('layouts.app')

@section('content')
	<div class="card">

		<div class="card-body">
		
			@if (session('message'))
		<div class="alert alert-success">{{session('message')}}</div>
		@endif
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle-text-muted">Like: {{count($post->likes)}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains('user_id', Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
						@else
						<button type="submit" class="btn btn-success">Like</button>
						@endif
				</form>
			@endif

			{{-- start here the comment modal --}}
			
			<form method="POST" action="/posts/{{$post->id}}/comment">	
				@csrf	
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
							  Comment
							</button>
						
			<!-- Button trigger modal -->
		
			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						{{-- <div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Laravel Bootstrap Modal Form Validation Example - NiceSnippets.com</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div> --}}
						<div class="modal-body">
							<div class="alert alert-danger" style="display:none"></div>
							<form class="image-upload" method="post" action="#" enctype="multipart/form-data">
								@csrf
								
							
								<div class="form-group">
									<label for="content">Comment</label>
									<textarea name="content" class="textarea form-control" id="content" cols="40" rows="5"></textarea>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-success" id="formSubmit">Send Comment</button>
						</div>
					</div>
				</div>
			</div>
			</form>
			{{-- ends here --}}
			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>
@endsection